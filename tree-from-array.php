<?php

/**
 * @param array $source
 * Source array of assoc arrays with child-parent relationships
 *
 * @param string $id_attr
 * Id of each assoc array
 *
 * @param string $parent_id_attr
 * Parent ID of each assoc array
 *
 * @param string $children_attr
 * Param in each assoc array to store child nodes
 *
 * @param mixed $root_parent_attr_value
 * Value to compare with $parent_id_attr and fond root nodes in $source array
 *
 * @param array $list
 * Internal var for recursion
 *
 * @param bool $need_preparing
 * Internal var for recursion
 *
 * @return array
 * Tree assoc array
 */
function getTreeFromArray(
    array $source,
    $id_attr = 'id',
    $parent_id_attr = 'parent_id',
    $children_attr = 'children',
    $root_parent_attr_value = 0,
    $list = [],
    $need_preparing = true
)
{
    if ($need_preparing) {
        $table_formatted = [];
        for ($i = 0; $i < count($source); $i++) {
            $node = $source[$i];
            $node[ $children_attr ] = [];
            $table_formatted[] = $node;
            if ( empty($node[$parent_id_attr]) || $node[$parent_id_attr] == $root_parent_attr_value )
                $list[] = $node;
        }
        $source = $table_formatted;
    }

    for ($i = 0; $i < count($list); $i++) {
        for ($j = 0; $j < count($source); $j++)
            if ( $list[$i][ $id_attr ] == $source[$j][ $parent_id_attr ] )
                $list[$i][ $children_attr ][] = $source[$j];
        if ( !empty($list[$i][ $children_attr ]) )
            $list[$i][ $children_attr ] = getTreeFromArray($source, $id_attr, $parent_id_attr, $children_attr,
                $root_parent_attr_value, $list[$i][ $children_attr ], false);
    }

    return $list;
}