<?php

class TreeFromArray
{
    public $id_attr = 'id';
    public $parent_id_attr = 'parent_id';
    public $children_attr = 'children';
    public $root_parent_attr_value = 0;

    private $tree_data = [];
    private $table_data = [];

    public function getTree($table_data = [])
    {
        if ( !empty($table_data) ) $this->table_data = $table_data;

        $this->formatSourceData();
        $this->addChildrenRecursive($this->tree_data);

        return $this->tree_data;
    }

    private function formatSourceData()
    {
        $table_formatted = [];
        for ($i = 0; $i < count($this->table_data); $i++) {
            $node = $this->table_data[$i];
            $node[ $this->children_attr ] = [];
            $table_formatted[] = $node;
            if ( empty($node[$this->parent_id_attr]) || $node[$this->parent_id_attr] == $this->root_parent_attr_value )
                $this->tree_data[] = $node;
        }
        $this->table_data = $table_formatted;
    }

    private function addChildrenRecursive(&$list)
    {
        for ($i = 0; $i < count($list); $i++) {
            for ($j = 0; $j < count($this->table_data); $j++)
                if ( $list[$i][ $this->id_attr ] == $this->table_data[$j][ $this->parent_id_attr ] )
                    $list[$i][ $this->children_attr ][] = $this->table_data[$j];
            if ( !empty($list[$i][ $this->children_attr ]) )
                $this->addChildrenRecursive($list[$i][ $this->children_attr ]);
        }
    }
}